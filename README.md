# To Install
(Re)move ~/.vim and ~/.vimrc if you have them already, and run:

    git clone git@bitbucket.org:jonpaul/vim-config.git
    cd ~/.vim
    git submodule update --init --recursive --remote
    ln -s ~/.vim/vimrc ~/.vimrc

# Basics
- Leader is mapped to `;`
- NerdTree show hide is mapped to `\`

# tmux support (optional)
If you will be using VIM with tmux for remote pairing or window management,
see the README at [https://github.com/pivotal/tmux-config](https://github.com/pivotal/tmux-config)
    
# Compile command-t

    rvm system #ensure build on Ruby 1.8.7 if using RVM
    cd ~/.vim/bundle/command-t
    bundle
    rake make

# Installing Eclim

See [http://eclim.org/install.html](http://eclim.org/install.html)

# Compile YouCompleteMe

    $ cd ~/.vim/bundle/vim-youcompleteme/
    $ ./install.sh --clang-completer --gocode-completer --tern-completer

# Compiling Ctags

    ;rt

# Updating
Update submodules with:

    git submodule foreach git pull origin master

