command! Rroutes :R config/routes.rb
command! RSroutes :RS config/routes.rb
command! Rseed :R db/seeds.rb
command! RSseed :RS db/seeds.rb
autocmd BufNewFile,BufRead *.mobile.erb let b:eruby_subtype='html'
autocmd BufNewFile,BufRead *.mobile.erb set filetype=eruby
autocmd BufNewFile,BufReadPost *.coffee setl shiftwidth=2 expandtab
