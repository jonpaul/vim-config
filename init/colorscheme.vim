" Turn on Solarized 256 colors if the terminal supports it.
" (Why Solarized doesn't do this properly on its own is unknown.)
if &t_Co == 256
  let g:solarized_termcolors=256
endif

set background=dark
colorscheme NeoSolarized
let g:neosolarized_contrast = "high"
let g:neosolarized_visibility = "normal"
let g:neosolarized_bold = 1
let g:neosolarized_underline = 1
let g:neosolarized_italic = 1

hi CursorLine guifg=NONE guibg=NONE
