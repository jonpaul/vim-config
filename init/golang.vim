let g:syntastic_go_checkers = ['goimports', 'golint', 'govet', 'errcheck', 'gofmt', 'gotype']
let g:syntastic_mode_map = { 'mode': 'active', 'passive_filetypes': ['go'] }
let g:go_list_type = "quickfix"

let g:go_guru_scope = ["...", "-github.mb-internal.com/retina/vendor"]

map gm :GoImplements<CR>
